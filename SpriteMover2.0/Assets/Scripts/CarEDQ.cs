﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarEDQ : MonoBehaviour
{
    //Creating references to the objects we will be manipulating
    //inside this file, the script and the sprite.
    public GameObject RedCar;
    public CarMove movement;

	void Start ()
    {

	}
	
	void Update ()
    {
        //linking our reference to our movement script to the
        //actual script, allowing us to affect it.
        movement = RedCar.GetComponent<CarMove>();

        //We will use P to 'toggle' the activation of the script
        //so we can turn it back on when we want to.
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("P, Script Toggled");
            movement.enabled = !movement.enabled;
        }

        //We will use Tab to 'toggle' the sprite, we can turn
        //it on and off as we like, we put this script on a 
        //seperate object for this reason.
		if (Input.GetKeyDown(KeyCode.Tab))
        {
            Debug.Log("Tab, Sprite Disabled");
            RedCar.SetActive(!RedCar.activeInHierarchy);
        }

        //Created the 'clean close' option for if this game is ever
        //built, simply press Esc and the program will close.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Escape, Application Exit");
            Application.Quit();
        }
	}

}
